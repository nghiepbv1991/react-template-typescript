module.exports = {
  root: true,
  plugins: ['react', 'prettier', 'react-hooks', '@typescript-eslint'],
  extends: [
    'airbnb-typescript',
    'prettier',
    'plugin:import/recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'plugin:prettier/recommended',
    'plugin:unicorn/recommended',
  ],
  rules: {
    'prettier/prettier': [
      'error',
      {
        arrowParens: 'avoid',
        semi: true,
        trailingComma: 'all',
        endOfLine: 'lf',
        tabWidth: 2,
        useTabs: false,
        singleQuote: true,
        printWidth: 80,
        jsxSingleQuote: true,
      },
    ],
    'react/jsx-filename-extension': [0, { extensions: ['.tsx'] }],
    'react-hooks/rules-of-hooks': 'error',
    '@typescript-eslint/no-unused-vars': 'warn',
    '@typescript-eslint/default-param-last': ['warn'],
    '@typescript-eslint/no-unsafe-assignment': 'warn',
    'unicorn/filename-case': [
      'error',
      {
        cases: {
          camelCase: true,
          pascalCase: true,
        },
        ignore: ['App.tsx'],
      },
    ],
    'unicorn/no-array-for-each': 'off',
    'unicorn/no-array-reduce': 'off',
    'unicorn/prevent-abbreviations': 'off',
    'unicorn/no-null': 'off',
    'unicorn/consistent-function-scoping': 'off',
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: './tsconfig.json',
  },
  settings: {
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts', '.tsx'],
    },
    'import/resolver': {
      typescript: {},
    },
  },
  overrides: [
    {
      files: ['src/components/**/*.tsx'],
      rules: {
        'unicorn/filename-case': [
          'error',
          {
            case: 'pascalCase',
          },
        ],
      },
    },
    {
      files: ['src/pages/**/*.tsx', 'react-app-env.d.ts'],
      rules: {
        'unicorn/filename-case': [
          'error',
          {
            case: 'kebabCase',
          },
        ],
      },
    },
  ],
};
