export interface IUserProfile {
  id?: number;
  address?: string;
  bib?: string;
  birthday?: string;
  districtId?: number;
  districtName?: string;
  email?: string;
  fullName?: string;
  gender?: 0 | 1 | null;
  height?: number;
  image?: string;
  nationalId?: string;
  phoneNumber?: string;
  provinceId?: number;
  provinceName?: string;
  shirtSize?: string;
  wardId?: number;
  wardName?: string;
  weight?: number;
}
export interface ILogin {
  accesstoken?: string;
  expiresIn?: number;
  created?: number;
  tokenType?: string;
  refresh_token?: string;
  lang?: 'vi' | 'en';
}
export interface ILoginPayload {
  email: string;
  password: string;
}
