import { all } from 'redux-saga/effects';
import authSaga from '../features/auth/authSaga';
import counterSaga from '../features/counter/counterSaga';

function* rootSaga() {
  console.log('run root saga');
  yield all([authSaga(), counterSaga()]);
}
export default rootSaga;
