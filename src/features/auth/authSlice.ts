import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IUserProfile, ILoginPayload } from '../../models/users';
import { RootState } from '../../store';
export interface AuthState {
  isLoggedIn: boolean;
  logging?: boolean;
  currentUser?: IUserProfile;
}

const initialState: AuthState = {
  isLoggedIn: false,
  logging: false,
  currentUser: undefined,
};
const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login: (state, action: PayloadAction<ILoginPayload>) => {
      state.logging = true;
    },
    loginSuccess: (state, action: PayloadAction<IUserProfile>) => {
      state.isLoggedIn = true;
      state.logging = false;
      state.currentUser = action.payload;
    },
    loginFailed: state => {
      state.logging = false;
    },
    logout: state => {
      state.isLoggedIn = false;
      state.currentUser = undefined;
    },
  },
});
export const { login, loginSuccess, loginFailed, logout } = authSlice.actions;
export const selectIsLoggedIn = (state: RootState) => state.auth.isLoggedIn;
export const selectIsLogging = (state: RootState) => state.auth.logging;
export default authSlice.reducer;
