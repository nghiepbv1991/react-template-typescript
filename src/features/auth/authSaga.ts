import { PayloadAction } from '@reduxjs/toolkit';
import { call, fork, take } from 'redux-saga/effects';
import { ILoginPayload } from '../../models/users';
import { login, logout } from './authSlice';

function* handleLogin(action: PayloadAction<ILoginPayload>) {
  console.log('Handle login:', action);
  localStorage.setItem('access_token', 'xyz');
}
const delay = (ms: number) =>
  new Promise(resolve => {
    setTimeout(() => {
      resolve(localStorage.removeItem('access_token'));
    }, ms);
  });

function* handleLogout() {
  console.log('Handle logout');
  yield delay(2000);
}

function* watchLogin() {
  while (true) {
    console.log('watch login');
    const isLoggedIn = !!localStorage.getItem('access_token');
    if (!isLoggedIn) {
      const action: PayloadAction<ILoginPayload> = yield take(login.type);
      yield fork(handleLogin, action);
    }
    yield take([logout.type]);
    yield call(handleLogout);
  }
}
export default function* authSaga() {
  console.log('auth saga');
  yield fork(watchLogin);
}
