import * as React from 'react';
import { useAppDispatch } from '../../../hooks';
import { login } from '../authSlice';

export interface ILoginPageProps {
  name?: string;
}

export default function LoginPage(props: ILoginPageProps) {
  const dispatch = useAppDispatch();

  const handleLoginClick = () => {
    dispatch(
      login({
        email: 'nghiepbvptit@gmail.com',
        password: '20121991',
      }),
    );
  };

  return (
    <div>
      <button onClick={handleLoginClick}>Fake Login</button>
    </div>
  );
}
