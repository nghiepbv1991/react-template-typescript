import { combineReducers } from 'redux';
import userReducer, { UserPayload } from './users';
export type AppState = {
  user: UserPayload;
};
const rootReducer = combineReducers<AppState>({
  user: userReducer,
});
export default rootReducer;
