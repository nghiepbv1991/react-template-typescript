export type UserInfo = {
  email?: string;
  birthday?: {
    date: number;
    month: number;
    year: number;
  };
  followings?: string[];
  fullname?: string;
  phone?: string;
  username?: string;
  avatarURL?: string;
  bio?: string;
  website?: string;
  gender?: 0 | 1 | 2;
  storyNotificationList?: string[];
  postNotificationList?: string[];
  requestedList?: string[];
  unSuggestList?: string[];
};
export type UserPayload = {
  user: {
    logined?: boolean;
    userInfo?: UserInfo;
  };
};
const initialState: UserPayload = {
  user: {},
};
interface SuccessAction<T> {
  type: string;
  payload?: T;
}
export type UserAction = SuccessAction<UserInfo> | SuccessAction<undefined>;

const userReducer = (
  state: UserPayload = initialState,
  action: UserAction,
): UserPayload => {
  switch (action.type) {
    case 'GET_USER_SUCCESS': {
      return {
        ...state,
      };
    }

    default: {
      return state;
    }
  }
};

export default userReducer;
