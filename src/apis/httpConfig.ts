import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import queryString from 'query-string';
const camelize = require('camelize');
enum StatusCode {
  Unauthorized = 401,
  Forbidden = 403,
  TooManyRequests = 429,
  InternalServerError = 500,
}

const headers: Readonly<Record<string, string | boolean>> = {
  Accept: 'application/json',
  'Content-Type': 'application/json; charset=utf-8',
  'Access-Control-Allow-Credentials': true,
  'X-Requested-With': 'XMLHttpRequest',
};

let isRefreshing = false;
let failedQueue: {
  resolve: (value: unknown) => void;
  reject: (reason?: any) => void;
}[] = [];

const processQueue = (error: null, token: string = '') => {
  failedQueue.forEach(prom => {
    if (error) {
      prom.reject(error);
    } else {
      prom.resolve(token);
    }
  });

  failedQueue = [];
};

class Http {
  private instance: AxiosInstance | null = null;

  private get http(): AxiosInstance {
    return this.instance != null ? this.instance : this.initHttp();
  }

  initHttp() {
    const http = axios.create({
      baseURL: 'http://foxstep-api-dev.fpt.net', //envs.BACKEND_URL,
      headers: headers,
      paramsSerializer: params => queryString.stringify(params),
      timeout: 15000,
      withCredentials: true,
    });
    const currentExecutingRequests: any = {};

    http.interceptors.request.use(
      async (req: AxiosRequestConfig) => {
        let originalRequest = req;

        const token = localStorage.getItem('token');
        // if (token) {
        if (req.headers)
          req.headers.Authorization = `Bearer eyJhbGciOiJSUzI1NiJ9.eyJhdXRoIjoiUk9MRV9VU0VSIiwidXNlcl9pZCI6NjIsImNyZWF0ZWQiOjE2NDY5OTI4OTIsInRva2VuX3R5cGUiOiJVU0VSIiwiZW52IjoiZGV2IiwiaXNzIjoiYTM2YzMwNDliMzYyNDlhM2M5Zjg4OTFjYjEyNzI0M2MiLCJzdWIiOiJ1c2VyXzYyIiwiZXhwIjoxNjQ2OTk2NDkyfQ.JQchz7Peti-ik9slWwJ5GXNxFpqZAJ8s4Tod7KKRs35dLA70iLs_WARQuNVdCy1N0k-7_935ypJet8JVCtzjNthMa4SpFYkIG8TMORAyA0YTDTVCT8pYc45UMHfDfn6jSSTwirOhRJSWqUhb2hc55cAPfz3v-f90uZS97FU8yaJLFpV08MoGijdDeZVAmIWAi9nTR_n0mR7w91laH77LCJkCOXRNbaaO2DQcIWHbcTIhFdAf5wvVALtYMmJFDjOWiqmhQYg1hD8kjx-VxnFVlP30XV7n3uprF6oYjvEHEp5MK2jGlxGftrp23BhwdbM2JvmmtOVzMM-dIJD2ilkSKg`;
        // }

        if (currentExecutingRequests[req.url as keyof {}]) {
          const source: any = currentExecutingRequests[req.url as keyof {}];
          delete currentExecutingRequests[req.url as keyof {}];
          source.cancel();
        }

        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        originalRequest.cancelToken = source.token;
        currentExecutingRequests[req.url as keyof {}] = source;

        return originalRequest;
      },
      error => Promise.reject(error),
    );

    http.interceptors.response.use(
      response => {
        if (
          currentExecutingRequests[response.request.responseURL as keyof {}]
        ) {
          // here you clean the request
          delete currentExecutingRequests[
            response.request.responseURL as keyof {}
          ];
        }

        return camelize(response);
      },
      async err => {
        const originalRequest = err.config;

        if (axios.isCancel(err)) {
          // here you check if this is a cancelled request to drop it silently (without error)
          return new Promise(() => {});
        }

        if (currentExecutingRequests[originalRequest.url as keyof {}]) {
          // here you clean the request
          delete currentExecutingRequests[originalRequest.url as keyof {}];
        }

        if (
          err.response?.status &&
          err.response.status === 401 &&
          !originalRequest._retry
        ) {
          if (isRefreshing) {
            return new Promise(function (resolve, reject) {
              failedQueue.push({ resolve, reject });
            })
              .then(token => {
                originalRequest.headers['Authorization'] = `Bearer ${token}`;
                return http(originalRequest);
              })
              .catch(err => {
                return Promise.reject(err);
              });
          }

          originalRequest._retry = true;
          isRefreshing = true;

          const clientRefreshToken = localStorage.getItem('clientRefreshToken');
          const deviceId = localStorage.getItem('deviceId');

          return new Promise(function (resolve, reject) {
            axios
              .post(
                `${'envs.BACKEND_URL'}${'URL_AUTH'}/auth/api/authenticate/refresh`,
                {
                  refresh_token: clientRefreshToken,
                  device_id: deviceId,
                },
              )
              .then(res => {
                if (res.data.code === '412' || res.data.code === '424') {
                }
                if (res.data.access_token) {
                  http.defaults.headers.common[
                    'Authorization'
                  ] = `Bearer ${res.data.access_token}`;
                  originalRequest.headers[
                    'Authorization'
                  ] = `Bearer ${res.data.access_token}`;
                  processQueue(null, res.data.access_token);
                  resolve(http(originalRequest));
                }
              })
              .catch(err => {
                processQueue(err, undefined);
                reject(err);
              })
              .then(() => {
                isRefreshing = false;
              });
          });
        }

        return Promise.reject(err);
      },
    );

    this.instance = http;
    return http;
  }

  request<T = any, R = AxiosResponse<T>>(
    config: AxiosRequestConfig,
  ): Promise<R> {
    return this.http.request(config);
  }

  get<T = any, R = AxiosResponse<T>>(
    url: string,
    config?: AxiosRequestConfig,
  ): Promise<R> {
    return this.http.get<T, R>(url, config);
  }

  post<T = any, R = AxiosResponse<T>>(
    url: string,
    params?: T,
    config?: AxiosRequestConfig,
  ): Promise<R> {
    return this.http.post<T, R>(url, params, config);
  }

  put<T = any, R = AxiosResponse<T>>(
    url: string,
    params?: T,
    config?: AxiosRequestConfig,
  ): Promise<R> {
    return this.http.put<T, R>(url, params, config);
  }

  delete<T = any, R = AxiosResponse<T>>(
    url: string,
    config?: AxiosRequestConfig,
  ): Promise<R> {
    return this.http.delete<T, R>(url, config);
  }

  // Handle global app errors
  // We can handle generic app errors depending on the status code
  private handleError(error: { status: number }) {
    const { status } = error;

    switch (status) {
      case StatusCode.InternalServerError: {
        // Handle InternalServerError
        break;
      }
      case StatusCode.Forbidden: {
        // Handle Forbidden
        break;
      }
      case StatusCode.Unauthorized: {
        // Handle Unauthorized
        break;
      }
      case StatusCode.TooManyRequests: {
        // Handle TooManyRequests
        break;
      }
    }

    return Promise.reject(error);
  }
}

export const http = new Http();
