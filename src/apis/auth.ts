import { AxiosResponse } from 'axios';
import { ILogin, ILoginPayload } from '../models/users';
import { http } from './httpConfig';

export const loginUser = async (params: ILoginPayload): Promise<ILogin> => {
  const url = '/auth/api/authenticate';
  const { data } = await http.post<ILoginPayload, AxiosResponse<ILogin>>(
    url,
    params,
  );
  return data;
};
