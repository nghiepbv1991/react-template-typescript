import { AxiosResponse } from 'axios';
import { IUserProfile } from '../models/users';
import { http } from './httpConfig';

export const fetchUserInfo = async (): Promise<IUserProfile> => {
  const url = '/auth/api/user/profile';
  const { data } = await http.get<null, AxiosResponse<IUserProfile>>(url);
  return data;
};
