import * as React from 'react';

export interface INotFoundProps {
  name?: string;
}

export function NotFound(props: INotFoundProps) {
  return <div>NotFound</div>;
}
