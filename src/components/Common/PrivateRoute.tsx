import * as React from 'react';
import { Route, RouteProps, Navigate } from 'react-router-dom';

export interface IPrivateRouteProps {
  name?: string;
}

export function PrivateRoute(props: RouteProps) {
  const isLoggedIn = !!localStorage.getItem('access_token');
  if (!isLoggedIn) {
    return <Navigate to='/login' />;
  }
  return <Route {...props} />;
}
