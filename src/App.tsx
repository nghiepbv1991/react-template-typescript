import React, { useEffect } from 'react';
import { Route, Routes } from 'react-router-dom';
import { fetchUserInfo } from './apis/users';
import './App.css';
import { NotFound, PrivateRoute } from './components/Common';
import { AdminLayout } from './components/Layout';
import { logout } from './features/auth/authSlice';
import LoginPage from './features/auth/pages/LoginPage';
import { useAppDispatch } from './hooks';
function App() {
  const dispatch = useAppDispatch();

  useEffect(() => {
    void (async () => {
      try {
        const resUser = await fetchUserInfo();
        console.log('res:', resUser);
      } catch {}
    })();
  }, []);

  return (
    <>
      <button onClick={() => dispatch(logout())}>Logout</button>
      <Routes>
        <Route path='/login' element={<LoginPage />} />
        <Route
          path='/admin'
          element={<PrivateRoute element={<AdminLayout />} />}
        />
        <Route element={<NotFound />} />
      </Routes>
    </>
  );
}

export default App;
